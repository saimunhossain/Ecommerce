@extends('layouts.main')

@section('content')
	<div class="row">
		<h2 style="text-align: center;">Cart Items</h2>

	<table class="table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Size</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($cartItems as $cartItem)
			<tr>
				<td>{{ $cartItem->name }}</td>
				<td>${{ $cartItem->price }}</td>
				<td width="50px">
					{!! Form::open(['route' => ['cart.update', $cartItem->rowId], 'method' => 'PUT']) !!}
					<input type="number" name="qty" value="{{ $cartItem->qty }}">
				</td>

				<td>
					<div > {!! Form::select('size', ['small'=>'Small','medium'=>'Medium','large'=>'Large'] , $cartItem->options->has('size')?$cartItem->options->size:'' ) !!}
                    </div>
				</td>

				<td>
					<input style="float: left;" type="submit" value="Confirm Item" class="button small success">
					{!! Form::close() !!}
					<form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<input type="submit" class="button small alert" value="Delete">
					</form>
				</td>
			</tr>
			@endforeach

			<tr>
				<td></td>
				<td>
					Tax: ${{ Cart::tax() }}<br>
					Sub Total: ${{ Cart::subtotal() }}<br>
					<strong>Grand Total:</strong> ${{ Cart::total() }}
				</td>
				<td>Items: {{ Cart::count() }}</td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</table>

	<a href="{{ route('checkout.shipping') }}" class="button">Check Out</a>

	</div>
@endsection