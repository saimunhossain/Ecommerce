@extends('admin.layout.admin')

@section('content')

	<h3>Products</h3>

<table class="table">
	@forelse($products as $product)
	<td>
		<h4>Name of product:{{ $product->name }}</h4>
		<h4>Category:{{ count($product->category)?$product->category->name:"N/A" }}</h4>
		<form action="{{ route('product.destroy',$product->id) }}" method="POST">
			{{ csrf_field('DELETE') }}
			{{ method_field('DELETE') }}
			<input class="btn btn-danger btn-sm" type="submit" value="Delete">
		</form>
	</td>

	@empty

	<h3>No products</h3>
	
	@endforelse
</table>

@endsection