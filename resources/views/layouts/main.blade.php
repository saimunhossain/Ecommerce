<!doctype html>
<html class="no-js" lang="en" dir="ltr">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="x-ua-compatible" content="ie=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>
            @yield('title', 'Saimon t-Shirts')
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="{{ asset('dist/css/foundation.css') }}"/>
        <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}"/>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
    </head>
    <body>
        <div  class="top-bar">
            <div style="color:white" class="top-bar-left">
                <h4 class="brand-title">
                    <a href="{{ route('home') }}">
                        <i class="fa fa-cart-arrow-down" aria-hidden="true">
                        </i>
                       Saimon t-Shirts
                    </a>
                </h4>
            </div>
            <div class="top-bar-right">
                <ol class="menu">
                    <li>
                        <a href="{{ route('shirts') }}">
                            SHIRTS
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('cart.index') }}">
                            CONTACT
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('cart.index') }}">
                            <i class="fa fa-shopping-cart fa-2x" aria-hidden="true">
                            </i>
                            CART
                            <span class="alert badge">
                                {{ Cart::count() }}
                            </span>
                        </a>
                    </li>
                </ol>
            </div>
        </div>
    @yield('content')

<footer class="footer">
  <div class="row full-width">
    <p>Developed with Love by Saimon Anam &copy;2017</p>
  </div>
</footer>

    <script src="{{ asset('dist/js/vendor/jquery.js') }}"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        Stripe.setPublishableKey('pk_test_ZzA30l46mWwyaBZOKViSGKEU');
    </script>
    <script src="{{ asset('dist/js/app.js') }}"></script>
    </body>
</html>
